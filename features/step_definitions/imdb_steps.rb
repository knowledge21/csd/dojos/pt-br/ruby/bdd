  Dado('que eu acessei o imdb') do
    visit 'https://www.imdb.com/'
  end
  
  Quando('eu buscar por {string}') do |quemdirigiu|
    input = find_field 'suggestion-search'
    input.send_keys quemdirigiu, :enter
  end
  
  Quando('entrar na pagina do filme') do
    find('#main > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td.result_text > a').click
  end
  
  Então('o {string} deve ser exibido como diretor') do |nome_do_diretor|
    expect(page).to have_content('Director: ' + nome_do_diretor)
  end